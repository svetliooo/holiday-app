define('environment',["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    debug: true,
    testing: true
  };
});
define('main',['exports', './environment', 'aurelia-framework', 'aurelia-logging-console'], function (exports, _environment, _aureliaFramework, _aureliaLoggingConsole) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.configure = configure;

  var _environment2 = _interopRequireDefault(_environment);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  _aureliaFramework.LogManager.addAppender(new _aureliaLoggingConsole.ConsoleAppender());
  _aureliaFramework.LogManager.setLevel(_aureliaFramework.LogManager.logLevel.debug);

  Promise.config({
    longStackTraces: _environment2.default.debug,
    warnings: {
      wForgottenReturn: false
    }
  });

  _aureliaFramework.ViewLocator.prototype.convertOriginToViewUrl3 = function (origin) {
    var moduleId = origin.moduleId;
    var id = moduleId.endsWith('.js') ? moduleId.substring(0, moduleId.length - 3) : moduleId;
    return id.replace('scripts', 'templates') + '.html';
  };

  _aureliaFramework.ViewLocator.prototype.convertOriginToViewUrl2 = function (origin) {
    console.log(arguments);

    return origin.moduleId + '.html';
  };

  function configure(aurelia) {
    aurelia.use.standardConfiguration().feature('resources').developmentLogging();

    if (_environment2.default.debug) {
      aurelia.use.developmentLogging();
    }

    if (_environment2.default.testing) {
      aurelia.use.plugin('aurelia-testing');
    }

    aurelia.start().then(function () {
      return aurelia.setRoot('shell', document.body);
    });
  }
});
define('shell',['exports', 'aurelia-router', 'helper/userHelper', 'aurelia-event-aggregator'], function (exports, _aureliaRouter, _userHelper, _aureliaEventAggregator) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.App = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var App = exports.App = function () {
    App.inject = function inject() {
      return [_aureliaEventAggregator.EventAggregator];
    };

    function App(eventAggregator) {
      var _this = this;

      _classCallCheck(this, App);

      this.events = eventAggregator;

      this.isUserLoggedIn = _userHelper.userHelper.getUserToken() !== null;

      this.routes = [
        {route: '', moduleId: 'destination/destinationIndex/destinationIndex', title: 'Home', name: 'destination', visibleIfUserIsNotLoggedIn: true, visible: true},
        {route: 'destination/create', moduleId: 'destination/destinationCreate/destinationCreate', title: 'Create Destination', name: 'destination', visibleIfUserIsNotLoggedIn: false, visible: true},
        {route: 'destination/edit/:id', moduleId: 'destination/destinationEdit/destinationEdit', title: 'Edit Destination', name: 'destination', visibleIfUserIsNotLoggedIn: false, visible: false},
        {route: 'destination/show/:id', moduleId: 'destination/destinationShow/destinationShow', title: 'Show Destination', name: 'destination', visibleIfUserIsNotLoggedIn: false, visible: false},
        {route: 'user/userCreate', moduleId: 'user/userCreate/userCreate', title: 'Create User', name: 'user', visibleIfUserIsNotLoggedIn: false, visible: true},
        {route: 'sign/in', moduleId: 'sign/signIn/signIn', title: 'Sign In', name: 'sign', visibleIfUserIsNotLoggedIn: true, visible: true},
        {route: 'sign/out', moduleId: 'sign/signOut/signOut', title: 'Sign Out', name: 'sign', visibleIfUserIsNotLoggedIn: false, visible: true}
      ];

      this.events.subscribe('userStatusChange', function (response) {
        return _this.isUserLoggedIn = response;
      });
    }

    App.prototype.configureRouter = function configureRouter(config, router) {
      config.title = 'HolidayApp';

      var step = new AuthorizeStep();
      config.addAuthorizeStep(step);
      config.map(this.routes);

      this.router = router;
    };

    return App;
  }();

  var AuthorizeStep = function () {
    function AuthorizeStep() {
      _classCallCheck(this, AuthorizeStep);
    }

    AuthorizeStep.prototype.run = function run(navigationInstruction, next) {
      if (navigationInstruction.getAllInstructions().some(function (i) {
        return i.config.settings.auth;
      })) {
        var isLoggedIn = _userHelper.userHelper.getUserToken() !== null;

        if (isLoggedIn === false) {
          return next.cancel(new _aureliaRouter.Redirect('sign/in'));
        }
      }

      return next();
    };

    return AuthorizeStep;
  }();
});
define('event/event',["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var Event = exports.Event = function () {
    function Event() {
      _classCallCheck(this, Event);

      this.message = "Please Select a Contact.";
    }

    Event.prototype.activate = function activate() {
      console.log(arguments);
    };

    return Event;
  }();
});
define('helper/destinationHelper', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var instance = null;

  var destinationHelper = exports.destinationHelper = function () {
    function destinationHelper() {
      _classCallCheck(this, destinationHelper);

      if (!instance) {
        instance = this;
      }

      return instance;
    }

    destinationHelper.getUserLocation = function getUserLocation(successCallBack) {
      navigator.geolocation.getCurrentPosition(successCallBack, showError);

      function showError(error) {
        switch (error.code) {
          case error.PERMISSION_DENIED:
            console.warn('User denied the request for Geolocation.');
            break;
          case error.POSITION_UNAVAILABLE:
            console.warn('Location information is unavailable.');
            break;
          case error.TIMEOUT:
            console.warn('The request to get user location timed out.');
            break;
          case error.UNKNOWN_ERROR:
            console.warn('An unknown error occurred.');
            break;
        }
      }
    };

    return destinationHelper;
  }();
});
define('helper/userHelper',['exports', 'lib/http'], function (exports, _http) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.userHelper = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var instance = null;

  var userHelper = exports.userHelper = function () {
    function userHelper() {
      _classCallCheck(this, userHelper);

      if (!instance) {
        instance = this;
      }

      return instance;
    }

    userHelper.updateUserToken = function updateUserToken(response) {
      this.removeUserToken();

      localStorage.setItem('userToken', response.token);

      return response;
    };

    userHelper.removeUserToken = function removeUserToken() {
      localStorage.removeItem('userToken');
    };

    userHelper.getUserToken = function getUserToken() {
      return localStorage.hasOwnProperty('userToken') === true ? localStorage.getItem('userToken') : null;
    };

    userHelper.authUserByEmailAndPass = function authUserByEmailAndPass(email, password) {
      var result = _http.http.post('sign/in', {email: email, password: password});

      result.then(function (response) {
        return response.json();
      }).then(this.updateUserToken.bind(this)).then(this.updateUserData.bind(this));

      return result;
    };

    userHelper.updateUserData = function updateUserData(response) {
      localStorage.setItem('currentUserData', JSON.stringify(response.user));
    };

    userHelper.getCurrentUser = function getCurrentUser() {
      var user = localStorage.getItem('currentUserData');

      return JSON.parse(user);
    };

    return userHelper;
  }();
});
define('lib/http', ['exports', 'aurelia-fetch-client', 'helper/userHelper'], function (exports, _aureliaFetchClient, _userHelper) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.http = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var httpClient = new _aureliaFetchClient.HttpClient();

  httpClient.configure(function (config) {
    config.useStandardConfiguration().withDefaults({
      credentials: 'same-origin',
      headers: {
        'X-Requested-With': 'Fetch',
        'access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      cache: 'default',
      mode: 'cors'
    });
  });

  var http = exports.http = function () {
    function http() {
      _classCallCheck(this, http);
    }

    http.handleErrors = function handleErrors(response) {
      if (response.ok === false) {
        console.warn('We\'ve catch an error !', response.statusText);
      }
    };

    http.fetch = function fetch(url, params) {
      var method = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'get';

      var request = httpClient.fetch(url, {
        method: method,
        body: (0, _aureliaFetchClient.json)(params)
      });

      request.catch(this.handleErrors);

      return request;
    };

    http.post = function post(url, params) {
      var method = 'post';

      params.token = _userHelper.userHelper.getUserToken();

      var request = httpClient.fetch(url, {
        method: method,
        body: (0, _aureliaFetchClient.json)(params)
      });

      request.catch(this.handleErrors);

      return request;
    };

    http.get = function get(url, params) {
      var method = 'get';

      url += '/' + params.join('/');
      url += '?token=' + _userHelper.userHelper.getUserToken();

      var request = httpClient.fetch(url, {
        method: method
      });

      request.catch(this.handleErrors);

      return request;
    };

    http.put = function put(url, params, queryParams) {
      var method = 'put';

      url += '/' + queryParams.join('/');
      params.token = _userHelper.userHelper.getUserToken();

      var request = httpClient.fetch(url, {
        method: method,
        body: (0, _aureliaFetchClient.json)(params)
      });

      request.catch(this.handleErrors);

      return request;
    };

    http.delete = function _delete(url, params, queryParams) {
      var method = 'delete';

      url += '/' + queryParams.join('/');
      params.token = _userHelper.userHelper.getUserToken();

      var request = httpClient.fetch(url, {
        method: method,
        body: (0, _aureliaFetchClient.json)(params)
      });

      request.catch(this.handleErrors);

      return request;
    };

    return http;
  }();
});
define('resources/index',["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.configure = configure;
  function configure(config) {}
});
define('destination/destinationCreate/destinationCreate', ['exports', 'aurelia-router', 'lib/http', 'helper/destinationHelper'], function (exports, _aureliaRouter, _http, _destinationHelper) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.destinationCreate = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var destinationCreate = exports.destinationCreate = function () {
    destinationCreate.inject = function inject() {
      return [_aureliaRouter.Router];
    };

    function destinationCreate(router) {
      _classCallCheck(this, destinationCreate);

      this.router = router;

      this.skeleton = {
        name: '',
        description: '',
        location: ''
      };

      this.errors = {};
    }

    destinationCreate.prototype.getUserLocation = function getUserLocation() {
      var that = this;

      _destinationHelper.destinationHelper.getUserLocation(function (position) {
        that.skeleton.location = position.coords.latitude + "," + position.coords.longitude;
      });
    };

    destinationCreate.prototype.submitForm = function submitForm() {
      var location = this.skeleton.location.split(',');

      this.skeleton.latitude = location[0];
      this.skeleton.longitude = location[1];

      var result = _http.http.post('destination', this.skeleton);

      result.then(this.handleSuccess.bind(this)).catch(this.handleErrors.bind(this));
    };

    destinationCreate.prototype.handleSuccess = function handleSuccess(response) {
      console.log(response);
      this.router.navigate('');
    };

    destinationCreate.prototype.handleErrors = function handleErrors(response) {
      var _this = this;

      var result = response.json();

      result.then(function (res) {
        return _this.matchErrorsWithFields(res.errors);
      });
    };

    destinationCreate.prototype.matchErrorsWithFields = function matchErrorsWithFields(response) {
      var that = this;

      response.forEach(function (value) {
        that.errors[value.path] = value.message;
      });
    };

    return destinationCreate;
  }();
});
define('destination/destinationEdit/destinationEdit', ['exports', 'lib/http', 'aurelia-router'], function (exports, _http, _aureliaRouter) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.destinationEdit = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var destinationEdit = exports.destinationEdit = function () {
    destinationEdit.inject = function inject() {
      return [_aureliaRouter.Router];
    };

    function destinationEdit(router) {
      _classCallCheck(this, destinationEdit);

      this.router = router;
      this.skeleton = {};
    }

    destinationEdit.prototype.activate = function activate(params) {
      var result = _http.http.get('destination', [params.id]);

      result.then(function (response) {
        return response.json();
      }).then(function (response) {
        this.skeleton = response;
      }.bind(this));
    };

    destinationEdit.prototype.submitForm = function submitForm() {
      var result = _http.http.put('destination', this.skeleton, [this.skeleton._id]);

      result.then(function (response) {
        return response.json();
      }).then(this.handleSuccess.bind(this)).catch(this.handleErrors.bind(this));
    };

    destinationEdit.prototype.handleSuccess = function handleSuccess(response) {
      if (response.result === true) {
        this.router.navigate('destination/show/' + this.skeleton._id);
      } else {
        this.handleErrors(response);
      }
    };

    destinationEdit.prototype.handleErrors = function handleErrors(response) {
      var _this = this;

      var result = response.json();

      result.then(function (res) {
        return _this.matchErrorsWithFields(res.errors);
      });
    };

    destinationEdit.prototype.matchErrorsWithFields = function matchErrorsWithFields(response) {
      var that = this;

      response.forEach(function (value) {
        that.errors[value.path] = value.message;
      });
    };

    return destinationEdit;
  }();
});
define('destination/destinationIndex/destinationIndex', ['exports', 'lib/http', 'helper/destinationHelper'], function (exports, _http, _destinationHelper) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.destinationIndex = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var destinationIndex = exports.destinationIndex = function () {
    function destinationIndex() {
      _classCallCheck(this, destinationIndex);

      this.destinations = [];

      _destinationHelper.destinationHelper.getUserLocation(this.getDestinations.bind(this));
    }

    destinationIndex.prototype.getDestinations = function getDestinations(position) {
      var _this = this;

      var result = _http.http.get('destination', [position.coords.longitude, position.coords.latitude, 10]);

      result.then(function (response) {
        return response.json();
      }).then(function (response) {
        return _this.destinations = response;
      });
    };

    return destinationIndex;
  }();
});
define('destination/destinationShow/destinationShow', ['exports', 'lib/http', 'helper/userHelper', 'aurelia-router'], function (exports, _http, _userHelper, _aureliaRouter) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.destinationShow = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var destinationShow = exports.destinationShow = function () {
    destinationShow.inject = function inject() {
      return [_aureliaRouter.Router];
    };

    function destinationShow(router) {
      _classCallCheck(this, destinationShow);

      this.router = router;

      this.skeleton = {};
      this.comments = [];

      this.params = {
        comments: [],
        destRating: 0,
        destId: 0
      };

      this.parentContext = this;
      this.isUserLoggedIn = _userHelper.userHelper.getUserToken() !== null;
      this.isOwner = false;
    }

    destinationShow.prototype.activate = function activate(params) {
      this.params.destId = params.id;

      var result = _http.http.get('destination', [params.id]);

      result.then(function (response) {
        return response.json();
      }).then(this.handleSuccess.bind(this));

      return result;
    };

    destinationShow.prototype.handleSuccess = function handleSuccess(destination) {
      this.skeleton = destination;
      this.params.comments = destination.comments;
      this.params.destRating = destination.rating;

      this.isOwner = this.isUserLoggedIn === true && _userHelper.userHelper.getCurrentUser()._id === this.skeleton.owner;
    };

    destinationShow.prototype.destroy = function destroy() {
      var _this = this;

      var result = _http.http.delete('destination', {}, [this.skeleton._id]);

      result.then(function (response) {
        return response.json();
      }).then(function (response) {
        return _this.router.navigate('');
      });

      return result;
    };

    destinationShow.prototype.edit = function edit() {
      this.router.navigate('destination/edit/' + this.skeleton._id);
    };

    return destinationShow;
  }();
});
define('event/eventCreate/eventCreate',['exports', 'aurelia-fetch-client'], function (exports, _aureliaFetchClient) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.eventCreate = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var httpClient = new _aureliaFetchClient.HttpClient();

  var eventCreate = exports.eventCreate = function () {
    function eventCreate() {
      _classCallCheck(this, eventCreate);

      console.log('we are here');
    }

    eventCreate.prototype.submitForm = function submitForm() {
      return console.warn('we are here');
    };

    return eventCreate;
  }();
});
define('sign/signIn/signIn',['exports', 'lib/http', 'aurelia-router', 'aurelia-event-aggregator', 'helper/userHelper'], function (exports, _http, _aureliaRouter, _aureliaEventAggregator, _userHelper) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.SignIn = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var SignIn = exports.SignIn = function () {
    SignIn.inject = function inject() {
      return [_aureliaRouter.Router, _aureliaEventAggregator.EventAggregator];
    };

    function SignIn(router, eventAggregator) {
      _classCallCheck(this, SignIn);

      this.router = router;
      this.events = eventAggregator;

      this.isUserLoggedIn = _userHelper.userHelper.getUserToken() !== null;

      this.skeleton = {
        email: '',
        password: ''
      };

      this.errors = {};
    }

    SignIn.prototype.submitForm = function submitForm() {
      var result = _userHelper.userHelper.authUserByEmailAndPass(this.skeleton.email, this.skeleton.password);

      this.errors = {};

      result.then(this.handleSuccess.bind(this)).catch(this.handleErrors.bind(this));
    };

    SignIn.prototype.handleSuccess = function handleSuccess() {
      this.events.publish('userStatusChange', true);

      this.router.navigate('');
    };

    SignIn.prototype.handleErrors = function handleErrors(response) {
      var _this = this;

      var result = response.json();

      result.then(function (res) {
        return _this.matchErrorsWithFields(res.errors);
      });
    };

    SignIn.prototype.matchErrorsWithFields = function matchErrorsWithFields(response) {
      var that = this;

      response.forEach(function (value) {
        that.errors[value.path] = value.message;
      });
    };

    return SignIn;
  }();
});
define('sign/signOut/signOut', ['exports', 'aurelia-router', 'helper/userHelper', 'aurelia-event-aggregator', 'lib/http'], function (exports, _aureliaRouter, _userHelper, _aureliaEventAggregator, _http) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.signOut = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var signOut = exports.signOut = function () {
    signOut.inject = function inject() {
      return [_aureliaEventAggregator.EventAggregator];
    };

    function signOut(eventAggregator) {
      _classCallCheck(this, signOut);

      this.events = eventAggregator;
    }

    signOut.prototype.canActivate = function canActivate() {
      _http.http.post('sign/out', {});
      _userHelper.userHelper.removeUserToken();

      this.events.publish('userStatusChange', false);

      return new _aureliaRouter.Redirect('');
    };

    return signOut;
  }();
});
define('user/userCreate/userCreate',['exports', 'lib/http', 'aurelia-router'], function (exports, _http, _aureliaRouter) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.userCreate = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var userCreate = exports.userCreate = function () {
    userCreate.inject = function inject() {
      return [_aureliaRouter.Router];
    };

    function userCreate(router) {
      _classCallCheck(this, userCreate);

      this.router = router;

      this.skeleton = {
        name: '',
        email: '',
        password: ''
      };

      this.errors = {};
    }

    userCreate.prototype.submitForm = function submitForm() {
      var result = _http.http.post('user', this.skeleton);

      this.errors = {};

      result.then(function (response) {
        return response.json();
      }).then(this.handleSuccess.bind(this)).catch(this.handleErrors.bind(this));
    };

    userCreate.prototype.handleSuccess = function handleSuccess(response) {
      this.router.navigate('sign/in');
    };

    userCreate.prototype.handleErrors = function handleErrors(response) {
      var _this = this;

      var result = response.json();

      result.then(function (res) {
        return _this.matchErrorsWithFields(res.errors);
      });
    };

    userCreate.prototype.matchErrorsWithFields = function matchErrorsWithFields(response) {
      var that = this;

      response.forEach(function (value) {
        that.errors[value.path] = value.message;
      });
    };

    return userCreate;
  }();
});
define('widget/Comment/Comment', ['exports', 'lib/http'], function (exports, _http) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Comment = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var Comment = exports.Comment = function () {
    function Comment() {
      _classCallCheck(this, Comment);

      this.ratings = [1, 2, 3, 4, 5];
      this.rating = 0;
      this.content = '';
      this.destId = null;
      this.comments = [];
      this.destRating = 0;
    }

    Comment.prototype.toView = function toView() {
      return 'widget/' + this.name + '/' + this.name + '.html';
    };

    Comment.prototype.activate = function activate(params) {
      this.destId = params.destId;
      this.comments = params.comments;
      this.parentContext = params.parentContext;
    };

    Comment.prototype.submitForm = function submitForm() {
      var result = _http.http.post('comment', {
        destinationId: this.destId,
        date: new Date(),
        content: this.content,
        rating: this.rating
      });

      result.then(function (response) {
        return response.json();
      }).then(this.handleSuccess.bind(this)).catch(this.handleErrors.bind(this));

      return result;
    };

    Comment.prototype.handleSuccess = function handleSuccess(response) {
      this.comments.unshift(response.comment);
      this.parentContext.params.destRating = response.rating;

      this.comment = '';
      this.rating = 0;
    };

    Comment.prototype.handleErrors = function handleErrors(response) {
      var _this = this;

      var result = response.json();

      result.then(function (res) {
        return _this.matchErrorsWithFields(res.errors);
      });
    };

    Comment.prototype.matchErrorsWithFields = function matchErrorsWithFields(response) {
      var that = this;

      response.forEach(function (value) {
        that.errors[value.path] = value.message;
      });
    };

    return Comment;
  }();
});
define('text!shell.html', ['module'], function (module) {
  module.exports = "<template>\n  <!--<require from=\"localhost/HolidayApp/front/styles/bootstrap/bootstrap.css\"></require>-->\n\n  <nav class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\" style=\"right: 1325px;\">\n        <div repeat.for=\"route of routes\">\n          <div class=\"navbar-header\" show.bind=\"route.visible === true && (route.visibleIfUserIsNotLoggedIn === true || (isUserLoggedIn === true && route.visibleIfUserIsNotLoggedIn === false))\">\n                <a class=\"navbar-brand\" href.one-way=\"'#' + route.route\">\n                    <i class=\"fa fa-user\"></i>\n                    <span>${route.title}</span>\n                </a>\n            </div>\n        </div>\n    </nav>\n\n  <div class=\"container\" style=\"margin-top: 50px;\">\n        <div class=\"row\">\n            <router-view class=\"col-md-8\"></router-view>\n        </div>\n    </div>\n</template>\n";
});
define('text!event/event.html', ['module'], function(module) { module.exports = "<template>\r\n  <div class=\"no-selection text-center\">\r\n    <h2>${message}</h2>\r\n  </div>\r\n</template>"; });
define('text!destination/destinationCreate/destinationCreate.html', ['module'], function (module) {
  module.exports = "<template>\n  <h3>Create destination</h3>\n\n  <form submit.trigger=\"submitForm()\" style=\"margin-top:30px;\">\n    <div>\n      Name: <input type=\"text\" value.bind=\"skeleton.name\"/>\n      <span style=\"color: red\">${errors.name}</span>\n    </div>\n\n    <div>\n      Description:\n      <textarea value.bind=\"skeleton.description\"></textarea>\n      <span style=\"color: red\">${errors.description}</span>\n    </div>\n\n    <div>\n      Location: <input type=\"text\" value.bind=\"skeleton.location\"/>\n      <button click.trigger=\"getUserLocation()\">Get location</button>\n      <span style=\"color: red\">${errors.location}</span>\n    </div>\n\n    <input type=\"submit\" value=\"Create\"/>\n  </form>\n</template>\n";
});
define('text!destination/destinationEdit/destinationEdit.html', ['module'], function (module) {
  module.exports = "<template>\n  <h3>Edit destination</h3>\n\n  <form submit.trigger=\"submitForm()\" style=\"margin-top:30px;\">\n    <div>\n      Name: <input type=\"text\" value.bind=\"skeleton.name\"/>\n      <span style=\"color: red\">${errors.name}</span>\n    </div>\n\n    <div>\n      Description:\n      <textarea value.bind=\"skeleton.description\"></textarea>\n      <span style=\"color: red\">${errors.description}</span>\n    </div>\n\n    <div>\n      Location: <input type=\"text\" value.bind=\"skeleton.location\"/>\n      <button click.trigger=\"getUserLocation()\">Get location</button>\n      <span style=\"color: red\">${errors.location}</span>\n    </div>\n\n    <input type=\"submit\" value=\"Update\"/>\n  </form>\n</template>\n";
});
define('text!destination/destinationIndex/destinationIndex.html', ['module'], function (module) {
  module.exports = "<template>\n  <h3>List Destinations</h3>\n\n  <div style=\"margin-top: 20px;\" repeat.for=\"dest of destinations\">\n    <div >\n      Name: <a href=\"#destination/show/${dest._id}\">${dest.name}</a>\n      &nbsp;&nbsp;&nbsp;\n      <a href=\"#destination/edit/${dest._id}\">Edit</a>\n      <br/>\n      Description: ${dest.description}\n      <br/>\n      Rating: ${dest.rating.toString().substring(0, 4)}\n    </div>\n  </div>\n</template>\n";
});
define('text!destination/destinationShow/destinationShow.html', ['module'], function (module) {
  module.exports = "<template>\n  <h3>Show Destination</h3>\n\n  Name: ${skeleton.name}\n  &nbsp;&nbsp;&nbsp;\n  <span show.bind=\"isOwner === true\">\n    <a click.trigger=\"destroy()\">Destroy</a>  /\n    <a click.trigger=\"edit()\">Edit</a>\n  </span>\n\n  <br/>\n  Description: ${skeleton.description}\n  <br/>\n  Location: ${skeleton.location} <!-- show map here -->\n  <br/>\n  Rating: ${params.destRating.toString().substring(0, 4)}\n  <br/>\n\n  <ul>\n    <li repeat.for=\"val of params.comments\">\n      ${val.content}\n    </li>\n  </ul>\n\n  <compose view-model=\"widget/Comment/Comment\" model.bind=\"{comments: params.comments, destRating: changeRating, destId: params.destId, parentContext: parentContext}\" show.bind=\"isUserLoggedIn === true\"></compose>\n</template>\n";
});
define('text!event/eventCreate/eventCreate.html', ['module'], function (module) {
  module.exports = "<template>\n  <h3>Create event</h3>\n\n  <form submit.trigger=\"submitForm()\" style=\"margin-top:30px;\">\n    <div>\n      Title: <input type=\"text\" value.bind=\"skeleton.title\"/>\n      <span style=\"color: red\">${errors.title}</span>\n    </div>\n\n    <div>\n      Description:\n      <textarea value.bind=\"skeleton.description\"></textarea>\n      <span style=\"color: red\">${errors.description}</span>\n    </div>\n\n    <div>\n      Destination:\n      <select>\n        <option>Choose a destination</option>\n      </select>\n      <span style=\"color: red\">${errors.destination}</span>\n    </div>\n\n    <div>\n      Date Start: <input type=\"date\" value.bind=\"skeleton.dateStart\"/>\n      <span style=\"color: red\">${errors.dateStart}</span>\n    </div>\n\n    <div>\n      Date End: <input type=\"date\" value.bind=\"skeleton.dateEnd\"/>\n      <span style=\"color: red\">${errors.dateEnd}</span>\n    </div>\n\n    <input type=\"submit\" value=\"Create\"/>\n  </form>\n</template>\n";
});
define('text!sign/signIn/signIn.html', ['module'], function(module) { module.exports = "<template>\n  <div show.bind=\"isUserLoggedIn === true\">\n    You're already logged in, you might want to <a href=\"#sign/out\">log out</a> ?\n  </div>\n\n  <div show.bind=\"isUserLoggedIn === false\">\n    Hello dear user,\n    You have to log in to use the application !\n\n    <form submit.trigger=\"submitForm()\">\n      <div>\n        Email: <input type=\"email\" value.bind=\"skeleton.email\"/>\n        <span style=\"color: red\">${errors.email}</span>\n      </div>\n\n      <div>\n        Password: <input type=\"password\" value.bind=\"skeleton.password\"/>\n        <span style=\"color: red\">${errors.password}</span>\n      </div>\n\n      <input type=\"submit\" value=\"Log In\"/>\n    </form>\n  </div>\n</template>\n"; });
define('text!user/userCreate/userCreate.html', ['module'], function(module) { module.exports = "<template>\n  <h3>Create user</h3>\n\n  <form submit.trigger=\"submitForm()\" style=\"margin-top:30px;\">\n    <div>\n      Username: <input type=\"text\" value.bind=\"skeleton.name\"/>\n      <span style=\"color: red\">${errors.name}</span>\n    </div>\n\n    <div>\n      Email: <input type=\"email\" value.bind=\"skeleton.email\"/>\n      <span style=\"color: red\">${errors.email}</span>\n    </div>\n\n    <div>\n      Password: <input type=\"password\" value.bind=\"skeleton.password\"/>\n      <span style=\"color: red\">${errors.password}</span>\n    </div>\n\n    <input type=\"submit\" value=\"Create\"/>\n  </form>\n</template>\n"; });
define('text!widget/Comment/Comment.html', ['module'], function (module) {
  module.exports = "<template>\n  <form submit.trigger=\"submitForm()\">\n    <textarea value.two-way=\"content\"></textarea>\n\n    <select value.bind=\"rating\">\n      <option model.bind=\"0\">Choose...</option>\n      <option repeat.for=\"value of ratings\" model.bind=\"value\">${value}</option>\n    </select>\n    <input type=\"submit\" value=\"Add Comment\"/>\n  </form>\n</template>\n";
});
//# sourceMappingURL=app-bundle.js.map
