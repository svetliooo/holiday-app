/**
 * Created by sstefanov on 18-Nov-16.
 */
import {http} from 'lib/http';
import {Router} from 'aurelia-router'

export class destinationEdit {
  static inject() {
    return [Router];
  }

  constructor(router) {
    this.router = router;
    this.skeleton = {};
  }

  activate(params) {
    let result = http.get('destination', [params.id]);

    result
      .then(response => response.json())
      .then(function (response) {
        this.skeleton = response;
      }.bind(this));
  }

  submitForm() {
    let result = http.put('destination', this.skeleton, [this.skeleton._id]);

    result
      .then(response => response.json())
      .then(this.handleSuccess.bind(this))
      .catch(this.handleErrors.bind(this));
  }

  handleSuccess(response) {
    if (response.result === true) {
      this.router.navigate('destination/show/' + this.skeleton._id);
    } else {
      this.handleErrors(response);
    }
  }

  handleErrors(response) {
    var result = response.json();

    result.then(res => this.matchErrorsWithFields(res.errors));
  }

  matchErrorsWithFields(response) {
    var that = this;

    response.forEach(function (value) {
      that.errors[value.path] = value.message;
    });
  }
}
