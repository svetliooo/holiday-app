/**
 * Created by sstefanov on 18-Nov-16.
 */
import {http} from 'lib/http';
import {userHelper} from 'helper/userHelper';
import {Router} from 'aurelia-router';

export class destinationShow {
  static inject() {
    return [Router];
  }

  constructor(router) {
    this.router = router;

    this.skeleton = {};
    this.comments = [];

    this.params = {
      comments: [],
      destRating: 0,
      destId: 0
    };

    //this is for the comment's widget
    this.parentContext = this;
    this.isUserLoggedIn = userHelper.getUserToken() !== null;
    this.isOwner = false;
  }

  activate(params) {
    this.params.destId = params.id;

    let result = http.get('destination', [params.id]);

    result
      .then(response => response.json())
      .then(this.handleSuccess.bind(this));

    return result;
  }

  handleSuccess(destination) {
    this.skeleton = destination;
    this.params.comments = destination.comments;
    this.params.destRating = destination.rating;

    this.isOwner = this.isUserLoggedIn === true && userHelper.getCurrentUser()._id === this.skeleton.owner;
  }

  destroy() {
    let result,
      confirmation = confirm('are you sure you want to delete this destination ?');

    if (confirmation === true) {
      result = http.delete('destination', {}, [this.skeleton._id]);

      result
        .then(response => response.json())
        .then(response => this.router.navigate(''));
    }

    return result;
  }

  edit() {
    this.router.navigate('destination/edit/' + this.skeleton._id);
  }
}
