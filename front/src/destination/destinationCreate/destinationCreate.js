/**
 * Created by sstefanov on 17-Nov-16.
 */
import {Router} from 'aurelia-router';
import {http} from 'lib/http';
import {destinationHelper} from 'helper/destinationHelper';

export class destinationCreate {
  static inject() {
    return [Router];
  }

  constructor(router) {
    this.router = router;

    this.skeleton = {
      name: '',
      description: '',
      location: ''
    };

    this.errors = {};
  }

  getUserLocation() {
    var that = this;

    destinationHelper.getUserLocation(function (position) {
      that.skeleton.location = position.coords.latitude + "," + position.coords.longitude;
    });
  }

  submitForm() {
    let location = this.skeleton.location.split(',');

    this.skeleton.latitude = location[0];
    this.skeleton.longitude = location[1];

    var result = http.post('destination', this.skeleton);

    result
    // .then(response => response.json())
      .then(this.handleSuccess.bind(this))
      .catch(this.handleErrors.bind(this));
  }

  handleSuccess(response) {
    console.log(response);
    this.router.navigate('');
  }

  handleErrors(response) {
    var result = response.json();

    result.then(res => this.matchErrorsWithFields(res.errors));
  }

  matchErrorsWithFields(response) {
    var that = this;

    response.forEach(function (value) {
      that.errors[value.path] = value.message;
    });
  }
}
