/**
 * Created by sstefanov on 17-Nov-16.
 */
import {http} from 'lib/http';
import {destinationHelper} from 'helper/destinationHelper';

export class destinationIndex {
  constructor() {
    this.destinations = [];

    destinationHelper.getUserLocation(this.getDestinations.bind(this));
  }

  getDestinations(position) {
    let result = http.get('destination', [position.coords.longitude, position.coords.latitude, 10]);

    result
      .then(response => response.json())
      .then(response => this.destinations = response)
  }
}
