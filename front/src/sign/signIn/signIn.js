/**
 * Created by sstefanov on 16-Nov-16.
 */

import {http} from 'lib/http';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';
import {userHelper} from 'helper/userHelper';

export class SignIn {
  static inject() {
    return [Router, EventAggregator];
  }

  constructor(router, eventAggregator) {
    this.router = router;
    this.events = eventAggregator;

    this.isUserLoggedIn = userHelper.getUserToken() !== null;

    this.skeleton = {
      email: '',
      password: ''
    };

    this.errors = {};
  }

  submitForm() {
    var result = userHelper.authUserByEmailAndPass(this.skeleton.email, this.skeleton.password);

    this.errors = {};

    result
    // .then(response => response.json())
      .then(this.handleSuccess.bind(this))
      .catch(this.handleErrors.bind(this));
  }

  handleSuccess() {
    this.events.publish('userStatusChange', true);

    this.router.navigate('');
  }

  handleErrors(response) {
    var result = response.json();

    result.then(res => this.matchErrorsWithFields(res.errors));
  }

  matchErrorsWithFields(response) {
    var that = this;

    response.forEach(function (value) {
      that.errors[value.path] = value.message;
    });
  }
}
