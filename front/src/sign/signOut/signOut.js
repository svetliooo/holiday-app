/**
 * Created by sstefanov on 17-Nov-16.
 */
import {Redirect} from 'aurelia-router';
import {userHelper} from 'helper/userHelper';
import {EventAggregator} from 'aurelia-event-aggregator';
import {http} from 'lib/http';

export class signOut {
  static inject() {
    return [EventAggregator];
  }

  constructor(eventAggregator) {
    this.events = eventAggregator;
  }

  canActivate() {
    http.post('sign/out', {});
    userHelper.removeUserToken();

    this.events.publish('userStatusChange', false);

    return new Redirect('')
  }
}
