import {HttpClient, json} from 'aurelia-fetch-client';
import {userHelper} from 'helper/userHelper';

let httpClient = new HttpClient();

httpClient.configure(config => {
  config
    .useStandardConfiguration()
    .withDefaults({
      credentials: 'same-origin',
      headers: {
        'X-Requested-With': 'Fetch',
        'access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      cache: 'default',
      mode: 'cors'
    });
});

export class http {
  static handleErrors(response) {
    if (response.ok === false) {
      console.warn('We\'ve catch an error !', response.statusText);
    }
  }

  static fetch(url, params, method = 'get') {
    var request = httpClient.fetch(url, {
      method: method,
      body: json(params)
    });

    request.catch(this.handleErrors);

    return request;
  }

  static post(url, params) {
    var method = 'post';

    params.token = userHelper.getUserToken();

    var request = httpClient.fetch(url, {
      method: method,
      body: json(params)
    });

    request.catch(this.handleErrors);

    return request;
  }

  static get(url, params) {
    var method = 'get';

    url += '/' + params.join('/');
    url += '?token=' + userHelper.getUserToken();

    var request = httpClient.fetch(url, {
      method: method
    });

    request.catch(this.handleErrors);

    return request;
  }

  static put(url, params, queryParams) {
    var method = 'put';

    url += '/' + queryParams.join('/');
    params.token = userHelper.getUserToken();

    var request = httpClient.fetch(url, {
      method: method,
      body: json(params)
    });

    request.catch(this.handleErrors);

    return request;
  }

  static delete(url, params, queryParams) {
    var method = 'delete';

    url += '/' + queryParams.join('/');
    params.token = userHelper.getUserToken();

    var request = httpClient.fetch(url, {
      method: method,
      body: json(params)
    });

    request.catch(this.handleErrors);

    return request;
  }
}
