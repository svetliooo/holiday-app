/**
 * Created by sstefanov on 15-Nov-16.
 */
import {http} from 'lib/http';
import {Router} from 'aurelia-router';

export class userCreate {
  static inject() {
    return [Router];
  }

  constructor(router) {
    this.router = router;

    this.skeleton = {
      name: '',
      email: '',
      password: ''
    };

    this.errors = {};
  }

  submitForm() {
    var result = http.post('user', this.skeleton);

    this.errors = {};

    result
      .then(response => response.json())
      .then(this.handleSuccess.bind(this))
      .catch(this.handleErrors.bind(this));
  }

  handleSuccess(response) {
    this.router.navigate('sign/in');
  }

  handleErrors(response) {
    var result = response.json();

    result.then(res => this.matchErrorsWithFields(res.errors));
  }

  matchErrorsWithFields(response) {
    var that = this;

    response.forEach(function (value) {
      that.errors[value.path] = value.message;
    });
  }
}
