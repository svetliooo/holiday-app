/**
 * Created by sstefanov on 18-Nov-16.
 */
import {http} from 'lib/http';
//extend baseWidget so all widget could have same view's path
export class Comment {
  constructor() {
    this.ratings = [1, 2, 3, 4, 5];
    this.rating = 0;
    this.content = '';
    this.destId = null;
    this.comments = [];
    this.destRating = 0;
  }

  toView() {
    return 'widget/' + this.name + '/' + this.name + '.html';
  }

  activate(params) {
    this.destId = params.destId;
    this.comments = params.comments;
    this.parentContext = params.parentContext;
  }

  submitForm() {
    let result = http.post('comment', {
      destinationId: this.destId,
      date: new Date(),
      content: this.content,
      rating: this.rating
    });

    result
      .then(response => response.json())
      .then(this.handleSuccess.bind(this))
      .catch(this.handleErrors.bind(this));

    return result;
  }

  handleSuccess(response) {
    this.comments.unshift(response.comment);
    this.parentContext.params.destRating = response.rating;

    this.comment = '';
    this.rating = 0;
  }

  handleErrors(response) {
    var result = response.json();

    result.then(res => this.matchErrorsWithFields(res.errors));
  }

  matchErrorsWithFields(response) {
    var that = this;

    response.forEach(function (value) {
      that.errors[value.path] = value.message;
    });
  }
}
