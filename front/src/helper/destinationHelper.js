/**
 * Created by sstefanov on 17-Nov-16.
 */
let instance = null;

export class destinationHelper {
  constructor() {
    if (!instance) {
      instance = this;
    }

    return instance;
  }

  static getUserLocation(successCallBack) {
    navigator.geolocation.getCurrentPosition(successCallBack, showError);

    function showError(error) {
      switch (error.code) {
        case error.PERMISSION_DENIED:
          console.warn('User denied the request for Geolocation.');
          break;
        case error.POSITION_UNAVAILABLE:
          console.warn('Location information is unavailable.');
          break;
        case error.TIMEOUT:
          console.warn('The request to get user location timed out.');
          break;
        case error.UNKNOWN_ERROR:
          console.warn('An unknown error occurred.');
          break;
      }
    }
  }
}
