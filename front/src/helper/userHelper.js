/**
 * Created by sstefanov on 16-Nov-16.
 */
import {http} from 'lib/http';
let instance = null;

export class userHelper {
  constructor() {
    if (!instance) {
      instance = this;
    }

    return instance;
  }

  static updateUserToken(response) {
    this.removeUserToken();

    localStorage.setItem('userToken', response.token);

    return response;
  }

  static removeUserToken() {
    localStorage.removeItem('userToken');
  }

  static getUserToken() {
    return localStorage.hasOwnProperty('userToken') === true ? localStorage.getItem('userToken') : null;
  }

  static authUserByEmailAndPass(email, password) {
    var result = http.post('sign/in', {email: email, password: password});

    result
      .then(response => response.json())
      .then(this.updateUserToken.bind(this))
      .then(this.updateUserData.bind(this));

    return result;
  }

  static updateUserData(response) {
    localStorage.setItem('currentUserData', JSON.stringify(response.user));
  }

  static getCurrentUser() {
    let user = localStorage.getItem('currentUserData');

    return JSON.parse(user);
  }
}
