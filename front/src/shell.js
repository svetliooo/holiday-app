import {Redirect} from 'aurelia-router';
import {userHelper} from 'helper/userHelper';
import {EventAggregator} from 'aurelia-event-aggregator';

export class App {
  static inject() {
    return [EventAggregator];
  }

  constructor(eventAggregator) {
    this.events = eventAggregator;

    this.isUserLoggedIn = userHelper.getUserToken() !== null;

    this.routes = [
      {route: '', moduleId: 'destination/destinationIndex/destinationIndex', title: 'Home', name: 'destination', visibleIfUserIsNotLoggedIn: true, visible: true},
      {route: 'destination/create', moduleId: 'destination/destinationCreate/destinationCreate', title: 'Create Destination', name: 'destination', visibleIfUserIsNotLoggedIn: false, visible: true},
      {route: 'destination/edit/:id', moduleId: 'destination/destinationEdit/destinationEdit', title: 'Edit Destination', name: 'destination', visibleIfUserIsNotLoggedIn: false, visible: false},
      {route: 'destination/show/:id', moduleId: 'destination/destinationShow/destinationShow', title: 'Show Destination', name: 'destination', visibleIfUserIsNotLoggedIn: false, visible: false},
      {route: 'user/userCreate', moduleId: 'user/userCreate/userCreate', title: 'Create User', name: 'user', visibleIfUserIsNotLoggedIn: false, visible: true},
      {route: 'sign/in', moduleId: 'sign/signIn/signIn', title: 'Sign In', name: 'sign', visibleIfUserIsNotLoggedIn: true, visible: true},
      {route: 'sign/out', moduleId: 'sign/signOut/signOut', title: 'Sign Out', name: 'sign', visibleIfUserIsNotLoggedIn: false, visible: true}
    ];

    this.events.subscribe('userStatusChange', response => this.isUserLoggedIn = response);
  }

  configureRouter(config, router) {
    config.title = 'HolidayApp';

    var step = new AuthorizeStep;
    config.addAuthorizeStep(step);
    config.map(this.routes);

    this.router = router;
  }
}

class AuthorizeStep {
  run(navigationInstruction, next) {
    if (navigationInstruction.getAllInstructions().some(i => i.config.settings.auth)) {
      var isLoggedIn = userHelper.getUserToken() !== null;

      if (isLoggedIn === false) {
        return next.cancel(new Redirect('sign/in'));
      }
    }

    return next();
  }
}
