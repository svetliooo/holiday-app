import environment from './environment';
import {ViewLocator} from 'aurelia-framework';
import {LogManager} from 'aurelia-framework';
import {ConsoleAppender} from 'aurelia-logging-console';

LogManager.addAppender(new ConsoleAppender());
LogManager.setLevel(LogManager.logLevel.debug);
//Configure Bluebird Promises.
Promise.config({
  longStackTraces: environment.debug,
  warnings: {
    wForgottenReturn: false
  }
});

ViewLocator.prototype.convertOriginToViewUrl3 = (origin) => {
    let moduleId = origin.moduleId;
    let id = moduleId.endsWith('.js') ? moduleId.substring(0, moduleId.length - 3) : moduleId;
    return `${id.replace('scripts', 'templates')}.html`;
};

ViewLocator.prototype.convertOriginToViewUrl2 = function (origin) {
  console.log(arguments);

  return origin.moduleId + '.html';
};

export function configure(aurelia) {
  aurelia.use
    .standardConfiguration()
    .feature('resources')
    .developmentLogging();


  if (environment.debug) {
    aurelia.use.developmentLogging();
  }

  if (environment.testing) {
    aurelia.use.plugin('aurelia-testing');
  }

  aurelia.start().then(() => aurelia.setRoot('shell', document.body));
}
