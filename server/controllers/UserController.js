/**
 * Created by Светослав on 21-Sep-15.
 */
var BaseController = require('./BaseController'),
    userHelper = require('../helpers/userHelper'),
    User = require('../models/User');

function UserController() {
    /**
     * Get user by id
     */
    this.showAction = function (req, res) {
        User.findById(req.params.id, function (err, user) {
            if (_.isNull(err)) {
                res.send(user);
            }
        });
    };

    /**
     * Create user
     */
    this.createAction = function (req, res) {
        delete req.body.token;

        var user = new User(req.body);

        user.save(function (err) {
            if (err) {
                return filterErrors(res, err, 500);
            }

            res.status(200).send(req.body);
        });
    };

    /**
     * List all users
     */
    this.indexAction = function (req, res) {
        User.find({}, function (err, users) {
            res.json(users);
        });
    };

    /**
     * Delete user by id
     */
    this.destroyAction = function (req, res) {
        User.findByIdAndRemove(req.params.id, function () {
            res.status(200).send({result: true});
        });
    };

    /**
     * Update current user's details
     */
    this.updateAction = function (req, res) {
        var i;

        for (i in req.body) {
            if (!_.isUndefined(userHelper.currentUser[i]) && _.indexOf(userHelper.accesibleFields, i) > -1) {
                userHelper.currentUser[i] = req.body[i];
            }
        }

        userHelper.currentUser.isUpdatingProfile = true;

        userHelper.currentUser.save(function (err) {
            userHelper.currentUser.isUpdatingPassword = false;

            if (err) {
                return filterErrors(res, err, 500);
            }

            res.status(200).send({result: true});
        });
    };
}

UserController.prototype = new BaseController();

module.exports = UserController;