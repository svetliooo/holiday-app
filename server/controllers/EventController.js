/**
 * Created by Светослав on 05-Oct-15.
 */
var BaseController = require('./BaseController'),
    Event = require('../models/Event'),
    eventHelper = require('../helpers/eventHelper'),
    userHelper = require('../helpers/userHelper');

function EventController() {
    /**
     * Get all events by destination's id
     */
    this.indexAction = function (req, res) {
        eventHelper.getByDestination(req.params.destId, this.getCurrentPage(req), res, res.send.bind(res));
    };

    /**
     * Create new event
     */
    this.createAction = function (req, res) {
        var newEntry = new Event(req.body);

        newEntry.save(function (err) {
            if (err) {
                return filterErrors(res, err, 500);
            }

            res.status(200).send(req.body);
        });
    };

    /**
     * Delete event by event id
     */
    this.destroyAction = function (req, res) {
        Event.findByIdAndRemove(req.params.id, function (err, deletedRecord) {
            var result = {result: false};

            if (!_.isNull(deletedRecord)) {
                result.result = true;
            }

            res.send(result);
        });
    };

    /**
     * Update event's information
     */
    this.updateAction = function (req, res) {
        delete req.body.owner;

        Event.findById(req.params.id, function (err, event) {
            event.filterUpdateData(req.body);

            event.update(
                {$set: event},
                {upsert: true}
            ).exec(function (err, result) {
                    if (err) {
                        return filterErrors(res, err, 500);
                    }

                    res.status(200).send({result: true});
                });
        });
    };

    /**
     * Show event's information
     */
    this.showAction = function (req, res) {
        Event.findById(req.params.id, function (err, event) {
            if (err) {
                return filterErrors(res, err, 500);
            }

            res.status(200).send(event);
        });
    };

    /**
     * Mark attending to event
     */
    this.attendInAction = function (req, res) {
        Event.findByIdAndUpdate(
            req.params.id,
            {$addToSet: {"attendees": userHelper.currentUser.id}},
            {safe: true, new: true},
            function (err, event) {
                if (err) {
                    return filterErrors(response, err, 500);
                }

                res.status(200).send(event);
            }
        );
    };

    /**
     * Mark NOT attending to event
     */
    this.attendOutAction = function (req, res) {
        Event.findByIdAndUpdate(
            req.params.id,
            {$pull: {"attendees": userHelper.currentUser.id}},
            {safe: true, new: true},
            function (err, event) {
                if (err) {
                    return filterErrors(response, err, 500);
                }

                res.status(200).send(event);
            }
        );
    };

    /**
     * Get events between two dates
     */
    this.betweenDatesAction = function (req, res) {
        var searchObject = {};

        if (!_.isUndefined(req.query.dateStart)) {
            searchObject.dateStart = {
                "$gte": new Date(parseInt(req.query.dateStart))
            };
        }

        if (!_.isUndefined(req.query.dateEnd)) {
            searchObject.dateEnd = {
                "$lt": new Date(parseInt(req.query.dateEnd))
            };
        }

        Event
            .find(searchObject)
            .sort({dateStart: -1})
            .limit(Constants.Comment.ITEMS_PER_PAGE)
            .skip(Constants.Comment.ITEMS_PER_PAGE * this.getCurrentPage(req))
            .exec(function (err, events) {
                if (err) {
                    return filterErrors(response, err, 500);
                }

                res.status(200).send(events);
            });
    };

    /**
     * Get all event, without any filtering ( like by destionation id )
     */
    this.getAllAction = function (req, res) {
        Event.find({}, function (err, events) {
            res.send(events);
        });
    };
}

EventController.prototype = new BaseController();

module.exports = EventController;