/**
 * Created by Светослав on 23-Sep-15.
 */
var BaseController = require('./BaseController'),
    User = require('../models/User'),
    userHelper = require('../helpers/userHelper');

function SignController() {
    /**
     * Sign in user by email and password
     */
    this.inAction = function (req, res) {
        userHelper.authUserByEmailAndPass(req.body.email, req.body.password, res, function () {
            userHelper.currentUser.token = userHelper.encryptString(req.body.email + req.body.password, userHelper.generateRandomNumber());
            userHelper.currentUser.isUpdatingProfile = true;

            var query = {email: req.body.email, password: userHelper.currentUser.password};
            var data = {token: userHelper.currentUser.token};

            User.update(query, data, {}, function (err) {
                userHelper.currentUser.isUpdatingPassword = false;

                if (_.isNull(err)) {
                    return res.status(200).send({
                        result: true,
                        token: userHelper.currentUser.token,
                        user: userHelper.getCurrentUser()
                    });
                }

                res.status(401).send('User not found');
            });
        });
    };

    /**
     * Sign out user
     */
    this.outAction = function (req, res) {
        userHelper.currentUser.token = null;

        userHelper.currentUser.save(function (err) {
            userHelper.currentUser = null;

            if (_.isNull(err)) {
                res.status(550).send('User has been logouted');
            }
        });
    };
}

SignController.prototype = new BaseController();

module.exports = new SignController();