/**
 * Created by Светослав on 21-Sep-15.
 */
function BaseController() {
    this.getCurrentPage = function (request) {
        var currentPage = 0;

        if (!_.isUndefined(request.body.page)) {
            currentPage = request.body.page;
        }

        if (!_.isUndefined(request.params.page)) {
            currentPage = request.params.page;
        }

        if (!_.isUndefined(request.query.page)) {
            currentPage = request.query.page;
        }

        return currentPage;
    };
}

module.exports = BaseController;