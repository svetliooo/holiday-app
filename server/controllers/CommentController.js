/**
 * Created by Светослав on 30-Sep-15.
 */
var BaseController = require('./BaseController'),
    Comment = require('../models/Comment'),
    commentHelper = require('../helpers/commentHelper'),
    userHelper = require('../helpers/userHelper'),
    destinationHelper = require('../helpers/destinationHelper');

function CommentsController() {
    /**
     * Create comment
     */
    this.createAction = function (req, res) {
        req.body.ownerId = userHelper.currentUser._id;
        var newEntry = new Comment(req.body);

        newEntry.save(function (err) {
            if (err) {
                return filterErrors(res, err, 500);
            }

            if (newEntry.rating > 0) {
                destinationHelper.updateRatingById(newEntry.destinationId, function (err, updatedDestination) {
                    res.status(200).send({
                        comment: req.body,
                        rating: updatedDestination.rating
                    });
                });
            } else {
                res.status(200).send(req.body);
            }
        });
    };

    /**
     * List all comments by destinationId
     */
    this.indexDestinationAction = function (req, res) {
        commentHelper.getCommentsByDestination(req.params.destId, this.getCurrentPage(req), res, res.send.bind(res));
    };

    /**
     * List all comments by ownerId
     */
    this.indexUserAction = function (req, res) {
        Comment.find({ownerId: userHelper.currentUser._id}, function (err, comments) {
            if (err) {
                return filterErrors(res, err, 500);
            }

            res.status(200).send(comments);
        });
    };

    /**
     * List all comments
     */
    this.indexAction = function (req, res) {
        Comment.find({}, function (err, comments) {
            res.send(comments);
        });
    };

    /**
     * Delete single comment
     */
    this.destroyAction = function (req, res) {
        Comment.findById(req.params.id, function (err, deletedComment) {
            if (err || _.isNull(deletedComment)) {
                return filterErrors(res, err, 500);
            }

            if (deletedComment.rating > 0) {
                deletedComment.remove(function () {
                    res.send({result: true});

                    destinationHelper.updateRatingById(deletedComment.destinationId, function () {
                    });
                });
            } else {
                deletedComment.remove(function () {
                    res.send({result: true});
                });
            }
        })
    };

    /**
     * Update comment
     */
    this.updateAction = function (req, res) {
        Comment.findById(req.params.id, function (err, comment) {
            if (!_.isNull(comment)) {
                comment.filterUpdateData(req.body);

                comment.save(function (err, result) {
                    if (err) {
                        return filterErrors(res, err, 500);
                    }

                    res.status(200).send(result);
                });
            } else {
                res.status(500).send({result: false});
            }
        });
    };
}

CommentsController.prototype = new BaseController();

module.exports = CommentsController;