/**
 * Created by Светослав on 21-Sep-15.
 */
var BaseController = require('./BaseController'),
    Destination = require('../models/Destination'),
    commentHelper = require('../helpers/commentHelper'),
    eventHelper = require('../helpers/eventHelper'),
    uploaderHelper = require('../helpers/uploaderHelper'),
    userHelper = require('../helpers/userHelper');

function DestinationController() {
    /**
     * Show single destination
     */
    this.showAction = function (req, res) {
        Destination.findById(req.params.id, function (err, destination) {
            commentHelper.getCommentsByDestination(destination.id, 1, res, function (comments) {
                destination.comments = comments;
                res.send(destination);

                // eventHelper.getByDestination(destination.id, 0, res, function (events) {
                //     destination.events = events;
                //
                //     res.send(destination);
                // });
            });
        });
    };

    /**
     * List all nearby destinations
     */
    this.indexAction = function (req, res) {
        var coordinates = [
                parseFloat(req.params.longitude),
                parseFloat(req.params.latitude)
            ],
            distance = parseInt(req.params.distance);

        Destination.find({
            location: {
                $near: {
                    $geometry: {
                        type: "Point",
                        coordinates: coordinates
                    },
                    $maxDistance: distance
                }
            }
        }).limit(Constants.Destination.ITEMS_PER_PAGE).exec(function (err, destinations) {
            if (err) {
                return filterErrors(res, err, 500);
            }

            res.send(destinations);
        });
    };

    /**
     * List all destinations without any filter
     */
    this.getAllAction = function (req, res) {
        Destination.find({}, function (err, destinations) {
            res.send(destinations);
        });
    };

    /**
     * Create destination
     */
    this.createAction = function (req, res) {
        req.body.location = [req.body.longitude, req.body.latitude];
        req.body.owner = userHelper.currentUser.id;

        var newEntry = new Destination(req.body);

        newEntry.save(function (err) {
            if (err) {
                return filterErrors(res, err, 500);
            }

            res.status(200).send(req.body);
        });
    };

    /**
     * Delete all destinations
     * DEBUG
     */
    this.destroyAllAction = function (req, res) {
        Destination.collection.remove(function () {
            res.status(200).send({result: true});
        });
    };

    /**
     * Delete single destination
     */
    this.destroyAction = function (req, res) {
        Destination.findByIdAndRemove(req.params.id, function (err, deletedRecord) {
            var result = {result: false};

            if (!_.isNull(deletedRecord)) {
                result.result = true;
            }

            res.send(result);
        });
    };

    /**
     * Update destination
     */
    this.updateAction = function (req, res) {
        delete req.body.rating;

        Destination.findById(req.params.id, function (err, destination) {
            destination.filterUpdateData(req.body);

            destination.update(
                {$set: destination},
                {upsert: true}
            ).exec(function (err, result) {
                if (err) {
                    return filterErrors(res, err, 500);
                }

                var result = {result: false};

                if (!_.isNull(result)) {
                    result.result = true;
                }

                res.send(result);
                // uploaderHelper.saveMultipleImages('picture', req, res, destination, Constants.Destination.THUMBNAILS, res.status(200).send.bind(res));
            });
        });
    };
}

DestinationController.prototype = new BaseController();

module.exports = DestinationController;