/**
 * Created by Светослав on 29-Sep-15.
 */

/**
 * Filter errors with proper response code
 */
global['filterErrors'] = function (response, error, errorCode) {
    var filter = [], i;

    if (!_.isUndefined(error.errors)) {
        for (i in error.errors) {
            filter.push({
                message: error.errors[i].message,
                kind: error.errors[i].kind,
                path: error.errors[i].path
            })
        }
    }

    if (!_.isUndefined(error.message) && filter.length < 1) {
        filter.push(error.message);
    }

    return response.status(errorCode).send({errors: filter});
};

global['generateRandomString'] = function (stringLength) {
    var text = "",
        possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < stringLength; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
};