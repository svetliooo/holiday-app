/**
 * Created by Светослав on 05-Oct-15.
 */
var mongoose = require('mongoose'),
    validate = require('mongoose-validator'),
    Schema = mongoose.Schema;

var dateValidator = [
    validate({
        validator: 'isDate',
        type: 'isDate',
        passIfEmpty: true
    })
];

var titleValidator = [
    validate({
        validator: 'isLength',
        type: 'isLength',
        arguments: [5, 60],
        message: 'Your comment should be between {ARGS[0]} and {ARGS[1]} characters long'
    })
];

var descriptionValidator = [
    validate({
        validator: 'isLength',
        type: 'isLength',
        passIfEmpty: true,
        arguments: [15, 500],
        message: 'Your comment should be between {ARGS[0]} and {ARGS[1]} characters long'
    })
];

var eventSchema = new Schema({
    title: {type: String, required: true, validate: titleValidator},
    description: {type: String, required: true, validate: descriptionValidator},
    owner: {type: Schema.Types.ObjectId, required: true, ref: 'User'},
    destinationId: {type: Schema.Types.ObjectId, required: true, ref: 'Destination'},
    dateStart: {type: Date, required: true, validate: dateValidator},
    dateEnd: {type: Date, required: false, validate: dateValidator},
    attendees: {type: Array, required: false}
});

eventSchema.path('description').validate(function (description, next) {
    next(description.match(/^[a-z\d\-_\s,.!?]+$/i))
});

eventSchema.methods = {
    filterUpdateData: function (data) {
        var i;

        for (i in data) {
            if (!_.isUndefined(this[i])) {
                this[i] = data[i];
            }
        }
    }
};
var saveCheck = function (next) {
    console.log(this.destinationId);
    next();
};

eventSchema.pre('save', saveCheck);
eventSchema.pre('update', saveCheck);

var event = mongoose.model('Event', eventSchema);

module.exports = event;