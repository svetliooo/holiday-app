/**
 * Created by Светослав on 21-Sep-15.
 */
var mongoose = require('mongoose'),
    userHelper = require('../helpers/userHelper'),
    validate = require('mongoose-validator'),
    Schema = mongoose.Schema;

//https://github.com/chriso/validator.js

var nameValidator = [
    validate({
        validator: 'isLength',
        arguments: [3, 40],
        type: 'isLength',
        message: 'Name should be between {ARGS[0]} and {ARGS[1]} characters'
    }),
    validate({
        validator: 'isAlpha',
        type: 'isAlpha',
        message: 'Name should contain alpha-numeric characters only'
    })
];

var emailValidator = [
    validate({
        validator: 'isEmail',
        type: 'isEmail'
    })
];

var passwordValidator = [
    validate({
        validator: 'isLength',
        arguments: [0, 30],
        type: 'isLength',
        message: 'Password should be between {ARGS[0]} and {ARGS[1]} characters'
    })
];

var UserSchema = new Schema({
    name: {type: String, required: true, validate: nameValidator},
    email: {type: String, required: true, validate: emailValidator},
    password: {type: String, required: true, validate: passwordValidator},
    token: {type: String, required: false}
});

UserSchema.path('email').validate(function (userEmail, next) {
    //Make sure the email address is not already registered
    User.findOne({'email': userEmail.toLowerCase()}, function (err, user) {
        next(err || _.isNull(user));
    });
}, 'Email is already registered');

UserSchema.methods = {};
UserSchema.isUpdatingProfile = false;

UserSchema.pre('save', function (next) {
    if (this.isNew === true || this.isUpdatingProfile === true) {
        this.password = userHelper.encryptString(this.password);
    }

    //next(new Error("Must specify the password confirmation"));
    next();
});

var User = mongoose.model('User', UserSchema);

module.exports = User;