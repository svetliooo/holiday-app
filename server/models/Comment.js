/**
 * Created by Светослав on 30-Sep-15.
 */
var mongoose = require('mongoose'),
    validate = require('mongoose-validator'),
    mongoosePaginate = require('mongoose-paginate'),
    Schema = mongoose.Schema;

var dateValidator = [
    validate({
        validator: 'isDate',
        type: 'isDate',
        passIfEmpty: true
    })
];

var contentValidator = [
    validate({
        validator: 'isLength',
        type: 'isLength',
        passIfEmpty: true,
        arguments: [10, 500],
        message: 'Your comment should be between {ARGS[0]} and {ARGS[1]} characters long'
    })
];

var ratingValidator = [
    validate({
        validator: 'isInt',
        type: 'isInt',
        passIfEmpty: true,
        options: {min: 0, max: 5}
    })
];

var CommentSchema = new Schema({
    ownerId: {type: Schema.Types.ObjectId, required: true, ref: 'User'},
    destinationId: {type: Schema.Types.ObjectId, required: true},
    date: {type: Date, required: true, validate: dateValidator},
    content: {type: String, required: false, validate: contentValidator},
    rating: {type: Number, required: false, default: 0, validate: ratingValidator},
    updateDate: {type: Date, required: false, default: null, validate: dateValidator}
});

CommentSchema.plugin(mongoosePaginate);

CommentSchema.path('content').validate(function (content, next) {
    next(content.length === 0 || content.match(/^[a-z\d\-_\s,.!?]+$/i))
});

CommentSchema.path('rating').validate(function (rating, next) {
    next(rating >= 0 && rating <= 5);
}, 'Rating should be between 0 and 5');

CommentSchema.methods = {
    filterUpdateData: function (data) {
        var i;

        delete data.ownerId;
        //delete data.destinationId;

        for (i in data) {
            if (!_.isUndefined(this[i])) {
                this[i] = data[i];
            }
        }

        var date = new Date();

        this.updateDate = date.getTime();
    }
};

CommentSchema.pre('save', function (next) {
    if (_.isEmpty(this.content) && this.rating === 0) {
        return next(new Error('content OR rating are required'));
    }

    next();
});

var Comment = mongoose.model('Comment', CommentSchema);

module.exports = Comment;