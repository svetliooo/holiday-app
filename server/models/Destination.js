/**
 * Created by Светослав on 21-Sep-15.
 */
var mongoose = require('mongoose'),
    validate = require('mongoose-validator'),
    Schema = mongoose.Schema;
//https://github.com/chriso/validator.js

var nameValidator = [
    validate({
        validator: 'isLength',
        type: 'isLength',
        passIfEmpty: false,
        arguments: [0, 50],
        message: 'Name should be between {ARGS[0]} and {ARGS[1]} characters long'
    }),
    validate({
        validator: 'isAlphanumeric',
        type: 'isAlphanumeric',
        message: 'Name should contain alpha-numeric characters only'
    })
];

var ratingValidator = [
    validate({
        validator: 'isFloat',
        type: 'isFloat',
        passIfEmpty: true,
        options: {min: 0, max: 5}
    })
];

var descriptionValidator = [
    validate({
        validator: 'isLength',
        type: 'isLength',
        passIfEmpty: false,
        arguments: [0, 500],
        message: 'Description should be between {ARGS[0]} and {ARGS[1]} characters long'
    })
];

var destinationSchema = new Schema({
    name: {type: String, required: true, validate: nameValidator},
    description: {type: String, required: true, validate: descriptionValidator},
    rating: {type: Number, required: false, default: 0, validate: ratingValidator},
    picture: {type: Array, required: false},
    location: {type: [Number], index: '2dsphere', required: true},
    comments: {type: Array, required: false, default: []},
    events: {type: Array, required: false, default: []},
    owner: {type: Schema.Types.ObjectId, required: true, ref: 'User'}
});

destinationSchema.path('description').validate(function (description, next) {
    next(description.match(/^[a-z\d\-_\s,.!?]+$/i))
});

destinationSchema.methods = {
    filterUpdateData: function (data) {
        var i;

        for (i in data) {
            if (!_.isUndefined(this[i])) {
                this[i] = data[i];
            }

            if (!_.isUndefined(data.longitude)) {
                this.location[0] = parseFloat(data.longitude);
            }

            if (!_.isUndefined(data.latitude)) {
                this.location[1] = parseFloat(data.latitude);
            }
        }
    }
};

destinationSchema.pre('save', function (next) {
    this.comments = [];
    this.events = [];
//next(new Error('nope'));
    next();
});

var destination = mongoose.model('Destination', destinationSchema);

module.exports = destination;