/**
 * Created by Светослав on 01-Oct-15.
 */
var Comment = require('../models/Comment'),
    mongoose = require('mongoose');

function commentHelper() {
    /**
     * Get comments by destination id
     * @param {ObjectId} destId
     * @param {Int} currentPage
     * @param {Object} response object
     * @param {Funtion} callback
     */
    this.getCommentsByDestination = function (destId, currentPage, response, callback) {
        Comment
            .paginate({destinationId: destId, rating: {$gt: 0}}, {
                sort: {date: -1},
                limit: Constants.Comment.ITEMS_PER_PAGE
            })
            .then(function (comments) {
                callback(comments.docs);
            });
    };

    this.getAverageRatingById = function (destId, callback) {
        //bug in mongoose - http://stackoverflow.com/questions/16310598/unable-to-use-match-operator-for-mongodb-mongoose-aggregation-with-objectid
        Comment.aggregate([
            {$match: {destinationId: new mongoose.Types.ObjectId(destId)}}
            ,{$group: {_id: null, "avgRating": {"$avg": {"$ifNull": ["$rating", 1]}}}}
        ], function (err, results) {
            var result = results.length > 0 ? results[0].avgRating : null;
            callback(result);
        })
    };
}

module.exports = new commentHelper();