/**
 * Created by Светослав on 01-Oct-15.
 */
var commentHelper = require('../helpers/commentHelper'),
    Destination = require('../models/Destination');

function destinationHelper() {
    this.updateRatingById = function (destId, callback) {
        Destination.findById(destId, function (err, destination) {
            commentHelper.getAverageRatingById(destId, function (averageRating) {
                if (!_.isNull(averageRating)) {
                    destination.rating = averageRating;

                    destination.save(callback);
                } else {
                    callback();
                }
            });
        });
    };
}

module.exports = new destinationHelper();