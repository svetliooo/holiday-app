/**
 * Created by Светослав on 23-Sep-15.
 */
var crypto = require('crypto');

var secureString = '&SDTA*&9d8stga7';

function userHelper() {
    this.currentUser = null;
    this.accesibleFields = ['name', 'email', 'password'];

    /**
     * Authenticate user by token
     *
     * @param {String} token
     * @param {Object} response
     * @param {Function} callback
     */
    this.authUserByToken = function (token, response, callback) {
        User.findOne({token: token}, function (err, user) {
            if (_.isNull(err) && !_.isNull(user)) {
                this.currentUser = user;
                return callback();
            }

            response.status(550).send('Token has expired');
        }.bind(this));
    };

    /**
     * Authenticate user by email and password
     *
     * @param {String} userEmail
     * @param {String} userPass
     * @param {Object} response
     * @param {Function} callback
     */
    this.authUserByEmailAndPass = function (userEmail, userPass, response, callback) {
        User.findOne({email: userEmail, password: this.encryptString(userPass, undefined)}, function (err, user) {
            if (_.isNull(err) && !_.isNull(user)) {
                this.currentUser = user;
                return callback(user);
            }

            response.status(550).send('User not found');
        }.bind(this));
    };

    /**
     * Encrypt string
     *
     * @param {String} content
     * @param {String} randomString
     * @return {String} | null
     * @api public
     */
    this.encryptString = function (content, randomString) {
        if (_.isUndefined(randomString)) {
            randomString = secureString;
        }

        try {
            return crypto
                .createHmac('sha1', randomString)
                .update(content)
                .digest('hex');
        } catch (err) {
            return null;
        }
    };

    /**
     * Generate random number
     */
    this.generateRandomNumber = function () {
        return Math.round((new Date().valueOf() * Math.random())) + '';
    };

    /**
     * Get current user safety - just some properties, not all of them
     * @return {Object}
     */
    this.getCurrentUser = function () {
        if (this.currentUser !== null) {
            return {
                _id: this.currentUser._id,
                name: this.currentUser.name
            };
        } else {
            return {};
        }
    };
}

module.exports = new userHelper();

var User = require('../models/User');