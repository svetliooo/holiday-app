/**
 * Created by Светослав on 05-Oct-15.
 */
var Event = require('../models/Event');

function eventHelper() {
    /**
     * Get events by destination id
     * @param {ObjectId} destId
     * @param {Int} currentPage
     * @param {Object} response object
     * @param {Funtion} callback
     */
    this.getByDestination = function (destId, currentPage, response, callback) {
        Event
            .find({destinationId: destId})
            .sort({'dateEnd': -1})
            .limit(Constants.Comment.ITEMS_PER_PAGE)
            .skip(Constants.Comment.ITEMS_PER_PAGE * currentPage)
            .exec(function (err, events) {
                if (err) {
                    return filterErrors(response, err, 500);
                }

                callback(events);
            });
    };
}

module.exports = new eventHelper();