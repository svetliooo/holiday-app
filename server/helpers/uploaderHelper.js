/**
 * Created by Светослав on 03-Oct-15.
 */
var multer = require('multer');
    // lwip = require('lwip');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, Constants.UPLOAD_DIR)
    },
    filename: function (req, file, cb) {
        cb(null, generateRandomString(15) + '_' + Date.now() + '.jpg')
    }
});

var imageMimetype = [
    'image/jpeg',
    'image/png'
];

function uploaderHelper() {
    this.isUploadingImages = false;
    /**
     * Save single file to the disk
     * @param {String} uploadField - upload field name
     * @param {Object} req - request object
     * @param {Object} res - response object
     * @param {Function} callback
     */
    function saveSingle(uploadField, req, res, callback) {
        upload.single(uploadField)(req, res, callback);
    };

    /**
     * Save multiple files to the disk
     * @param {String} uploadField - upload field name
     * @param {Object} req - request object
     * @param {Object} res - response object
     * @param {Function} callback
     */
    function saveMultiple(uploadField, req, res, callback) {
        upload.array(uploadField)(req, res, callback);
    };

    /**
     * Filter files by mimetype
     * @param {Object} req - request object
     * @param {Object} file - currently tested file
     * @param {Function} next
     */
    this.filterFiles = function (req, file, next) {
        if (this.isUploadingImages === true) {
            if (imageMimetype.indexOf(file.mimetype) > -1) {
                return next(null, true);
            } else {
                return next(new Error('This format is not supported !'));
            }
        }

        next(null, false);
    };

    /**
     * Save multiple images
     * @param {String} uploadField - upload field name
     * @param {Object} request - request object
     * @param {Object} response - response object
     * @param {Object} model - current model
     * @param {Array} thumbDimensions - width and height for thumbnails
     * @param {Function} callback
     */
    this.saveMultipleImages = function (uploadField, request, response, model, thumbDimensions, callback) {
        var i, a,
            that = this,
            imageFiles = [],
            modelField = uploadField,
            uploadField = uploadField + '[]';

        this.isUploadingImages = true;

        saveMultiple(uploadField, request, response, function (err) {
            if (err) {
                return filterErrors(response, err, 500);
            }

            for (i = 0; i < request.files.length; i++) {
                imageFiles.push(request.files[i].path);
                for (a = 0; a < thumbDimensions.length; a++) {
                    that.resizeImage(request.files[i].filename, thumbDimensions[a][0], thumbDimensions[a][1]);
                }
            }

            model[modelField] = imageFiles;
            model.save();

            callback({result: true});
        });
    };

    /**
     * Save single image
     * @param {String} uploadField - upload field name
     * @param {Object} request - request object
     * @param {Object} response - response object
     * @param {Object} model - current model
     * @param {Function} callback
     */
    this.saveSingleImage = function (uploadField, request, response, model, callback) {
        var that = this;

        this.isUploadingImages = true;

        saveSingle(uploadField, request, response, function (err) {
            if (err) {
                throw err;
            }

            model[uploadField] = request.file.path;

            that.resizeImage(request.file.filename, 200, 200);
            model.save();

            callback({result: true});
        });
    };

    /**
     * Resize image by dimensions
     * @param {String} imagePath - image's path
     * @param {Int} width - new image's width
     * @param {Int} height - new image's height
     */
    this.resizeImage = function (imagePath, width, height) {
        var newImagePath = Constants.UPLOAD_DIR + '\\' + width + '_' + height + '_' + imagePath;

        lwip.open(Constants.UPLOAD_DIR + '\\' + imagePath, function (err, image) {
            if (err) {
                throw err;
            }

            image.resize(width, height, 'nearest-neighbor', function (err, rzdImg) {
                rzdImg.writeFile(newImagePath, function (err) {
                    if (err) {
                        throw err;
                    }
                });
            });
        });
    };

    var settings = {
        dest: Constants.UPLOAD_DIR,
        limits: {
            files: 5,
            fileSize: 2 * 100 * 100 * 100 // 2 MB
            //fileSize: 78000 // 78 bytes
        },
        storage: storage,
        fileFilter: this.filterFiles.bind(this)
    };

    var upload = multer(settings);
}

module.exports = new uploaderHelper();