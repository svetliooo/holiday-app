/**
 * Created by Светослав on 21-Sep-15.
 */
var express = require('express'),
    userHelper = require('./helpers/userHelper');

function authUser(req, res, next) {
    var token = null;

    if (!_.isUndefined(req.body.token)) {
        token = req.body.token;
    }

    if (!_.isUndefined(req.params.token)) {
        token = req.params.token;
    }

    if (!_.isUndefined(req.query.token)) {
        token = req.query.token;
    }

    if (!_.isNull(token)) {
        return userHelper.authUserByToken(token, res, next);
    }

    // res.status(550).send('Token has expired');
    next();// only for debugging
};

function authNone(req, res, next) {
    next();
};

function router() {
    this.routes = [];

    this.routes.push({
        name: 'sign',
        subRoutes: [
            {router: '/in', type: 'post', action: 'in', auth: authNone},
            {router: '/out', type: 'post', action: 'out', auth: authUser}
        ]
    });

    this.routes.push({
        name: 'destination',
        subRoutes: [
            {router: '/:id', type: 'get', action: 'show', auth: authNone},
            {router: '/:longitude/:latitude/:distance', type: 'get', action: 'index', auth: authNone},
            {router: '', type: 'post', action: 'create', auth: authUser},
            {router: '', type: 'delete', action: 'destroyAll', auth: authNone},//debug
            {router: '/:id', type: 'delete', action: 'destroy', auth: authUser},
            {router: '/:id', type: 'put', action: 'update', auth: authUser},
            {router: '/get/all', type: 'get', action: 'getAll', auth: authNone}
        ]
    });

    this.routes.push({
        name: 'user',
        subRoutes: [
            {router: '/:id', type: 'get', action: 'show', auth: authUser},
            {router: '/:id', type: 'delete', action: 'destroy', auth: authUser},
            {router: '', type: 'post', action: 'create', auth: authNone},
            {router: '', type: 'get', action: 'index', auth: authNone},
            {router: '', type: 'put', action: 'update', auth: authUser}
        ]
    });

    this.routes.push({
        name: 'comment',
        subRoutes: [
            {router: '', type: 'post', action: 'create', auth: authUser},
            {router: '/dest/:destId', type: 'get', action: 'indexDestination', auth: authUser},
            {router: '/user', type: 'get', action: 'indexUser', auth: authUser},
            {router: '/:id', type: 'delete', action: 'destroy', auth: authUser},
            {router: '/:id', type: 'put', action: 'update', auth: authUser},
            {router: '', type: 'get', action: 'index', auth: authNone}//debug
        ]
    });

    this.routes.push({
        name: 'event',
        subRoutes: [
            {router: '', type: 'post', action: 'create', auth: authUser},
            {router: '/byDest/:destId', type: 'get', action: 'index', auth: authUser},
            {router: '/:id', type: 'delete', action: 'destroy', auth: authUser},
            {router: '/:id', type: 'put', action: 'update', auth: authUser},
            {router: '/:id', type: 'get', action: 'show', auth: authUser},
            {router: '/attendIn/:id', type: 'post', action: 'attendIn', auth: authUser},
            {router: '/attendOut/:id', type: 'post', action: 'attendOut', auth: authUser},
            {router: '/betweenDates/bla', type: 'get', action: 'betweenDates', auth: authUser},
            {router: '/get/all', type: 'get', action: 'getAll', auth: authNone}//debug
        ]
    });

    this.init = function (app) {
        var i, a, routerController, controller, controllerInstance;

        for (i = 0; i < this.routes.length; i++) {
            routerController = new express.Router();

            controller = this.includeController(this.routes[i].name);
            controllerInstance = typeof controller === 'function' ? new controller() : controller;

            for (a = 0; a < this.routes[i].subRoutes.length; a++) {
                routerController.route(this.routes[i].subRoutes[a].router)[this.routes[i].subRoutes[a].type](
                    this.routes[i].subRoutes[a].auth,
                    controllerInstance[this.routes[i].subRoutes[a].action + 'Action'].bind(controllerInstance)
                );
            }

            app.use('/' + this.routes[i].name, routerController);
        }
    };

    /**
     * Capitalize controller's name
     * @param {String} s - Controller's name
     */
    this.capitalizeControllerName = function (s) {
        return s[0].toUpperCase() + s.slice(1);
    };

    /**
     * Include controller by name
     * @param {String} controllerName
     */
    this.includeController = function (controllerName) {
        return require('./controllers/' + this.capitalizeControllerName(controllerName) + 'Controller');
    };
}

module.exports = new router();