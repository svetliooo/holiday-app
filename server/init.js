"use strict";

var express = require('express'),
    mongoose = require('mongoose'),
    router = require('./router'),
    bodyParser = require('body-parser');


GLOBAL._ = require('lodash');
GLOBAL.Constants = require('../constants.json');
require('./lib/functions');

var db = mongoose.connect('mongodb://localhost/exampleDb');
var app = express();
var port = process.env.PORT || 3000;

var frontEndDir = __dirname.replace('server', 'front');

app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(frontEndDir));
app.use(bodyParser.json());

app.get('/', function (req, res) {
    res.sendFile('index.html', {root: frontEndDir});
});

router.init(app);

app.listen(port, function () {
    console.log('\n\n RESTARTED !!! \n\n');
});

module.exports = app;